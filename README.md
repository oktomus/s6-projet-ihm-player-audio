### Compile from the terminal :v:

1. `cd` into the project directory;
2. run `qmake MPVPlayer.pro`, this will generate a Makefille;

	You can run this command only when the pro file has been modified.

3. run `make`, this will compile the code;
4. run `./MPVPlayer` to launch the project;
5. use `make clean` to remove all build-related files.

### Credits

This app is using [Dutranslator's stylesheet](https://github.com/Rainbox-dev/Dutranslator/blob/master/resources/styles/style.css) (a project I contributed to).
