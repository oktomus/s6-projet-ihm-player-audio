QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

INCLUDEPATH += ./taglib/include

LIBS += -L$$PWD/taglib/lib -ltag -lz

TEMPLATE = app

VPATH += ./src

SOURCES += main.cpp \
  interface.cpp \
    lecteur.cpp \
    playercommon.cpp \
    src/addmusicdialog.cpp \
    src/metroslider.cpp \
        potard.cpp

HEADERS  += interface.h \
    lecteur.h \
    playercommon.h \
    src/addmusicdialog.h \
    src/metroslider.h \
potard.h

FORMS    += src/interface.ui

RESOURCES += src/resources.qrc

TRANSLATIONS += src/app_en.ts


