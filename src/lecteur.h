/**
 * Kevin Masson
 * Projet IHM
 * L3 Informatique
 * S6 - Printemps 2017
 * Université de Strasbourg
 */
#ifndef LECTEUR_H
#define LECTEUR_H

#include "playercommon.h"
#include "interface.h"

#include <QObject>
#include <QLocalSocket>
#include <QFuture>
#include <QWaitCondition>


/**
 * @brief The Musique struct
 */
struct Musique {

    friend class Interface;

  QString path;
  int duration;
  int timePos;
  QString title, album, artist, date, genre;
  bool current;
  bool playing;

  Musique(){
      title = album = artist = date = genre = path = mediaUndefined;
      duration = -1;
      current = playing = false;
      timePos = 0;

  }

  int getDuration(){
    return duration;
  }


};


/**
 * @brief The actual player and its properties
 */
class Lecteur : public QObject
{

    Q_OBJECT

public:
    explicit Lecteur();
    ~Lecteur();


    /**
     * @brief Check if the player is in a pause state or not
     * @return
     */
    bool isPaused(){ return m_pause;}

    /**
     * @brief Check if the player is idle or not
     * @return
     */
    bool isPlaying();

    /**
     * @brief Check if the player is running, if not, the app should close
     * @return
     */
    bool isRunning(){ return m_receptionRunning;}


    /**
     * @brief Returns the player's volume
     * @return
     */
    int volume(){ return m_volume; }

    /**
     * @brief Get the musics list
     * @return
     */
    QList<Musique> musics(){ return m_musics; }

    /**
     * @brief Return the current music being listened
     * @return
     */
    Musique * getCurrentMusic();

signals:

    /**
     * @brief Emited when the player's state change
     */
    void playerUpdated(QString);

    /**
     * @brief Emited when a message must be printed to the user
     *
     * @param QString   The message title
     * @param QString   The message content
     * @param messageType The message type
     */
    void message(QString, QString, messageType);

    void quit();

public slots:

    /**
     * @brief Toggle between pause and play
     */
    void togglePlayPause();


    /**
     * @brief Change the player's volume
     * @param val
     */
    void changeVolume(int val);

    /**
     * @brief Change the time pos
     * @param pos Position in seconds
     */
    void changeTimePos(int pos);

    /**
     * @brief Open a dialog to add song
     */
    void addSongs();

    /**
     * @brief Play the next song
     */
    void next();

    /**
     * @brief Play the previous song
     */
    void previous();

    /**
     * @brief Move a bit in the timeline
     */
    void incTime();

    /**
     * @brief Move a bit to left in the timeline
     */
    void decTime();

    /**
     * @brief Initialise the player
     *
     * Muste be call only once
     */
    void init();

    /**
     * @brief Stop the current player client
     */
    void stop();

    /**
     * @brief Mute the player
     */
    void mute();

    /**
     * @brief Unmute the player
     */
    void unmute();

private:

    /**
     * @brief Socket used to communicate with mpv
     */
    QLocalSocket * m_socket;

    /**
     * @brief True if the player is paused, false otherwise
     */
    bool m_pause;

    /**
     * @brief Volume of the music, between 0 and 100
     */
    int m_volume;

    /**
     * @brief Last volume recorded when muted the player
     */
    int m_lastVolume;

    /**
     * @brief True while the player receive messages
     */
    bool m_receptionRunning;

    /**
     * @brief The id in m_musics of the current music being listened
     */
    int m_currentMusic;

    /**
     * @brief The list of the musics currently in the player
     */
    QList<Musique> m_musics;

    /**
     * @brief A unique id assigned at construction
     */
    qint64 m_id;


    /**
     * @brief A value is append each time the player is getting a data
     * Like this, the slot which reads the message knows what the data is related to
     * When the data is readed, the oldest value is removed
     */
    QList<QString> waitingData;

    /**
     * @brief Mutex used for sync
     */
    QMutex mutex;

    /**
     * @brief Thread on which messages will be receive
     */
    QFuture<void> m_messageLoopThread;

    /**
     * @brief Thread body which receive messages
     */
    void messageReceptionLoop();

    /**
     * @brief Get tags from a file
     * @param fileName
     * @return
     */
    QMap<QString, QString> getTags(QString fileName);

    /**
     * @brief Return properties of a music file
     * @param fileName
     * @return
     */
    QMap<QString, int> getProperties(QString fileName);

    /**
     * @brief Establish a connection to the socket of the mpv player
     */
    void connectOrLaunchMPV();

    /**
     * @brief Tells mpv to alert the model when their are updates
     */
    void observeUpdates();

    /**
     * @brief Update the player according to the data present in the socket
     */
    void updateFromSocket();

    /**
     * @brief Take the last message from the socket (if there is any)
     *
     * @return The received message
     */
    QJsonObject takeMessage();

    /**
     * @brief Parse a given message
     */
    void parseMessage(QJsonObject);

    /**
     * @brief Parse a playlist get property response
     */
    void parsePlaylistMessage(QJsonValue);

    /**
     * @brief Send the given json obj to the socket
     */
    void sendToSocket(QJsonObject);

};

#endif // LECTEUR_H
