/**
 * Kevin Masson
 * Projet IHM
 * L3 Informatique
 * S6 - Printemps 2017
 * Université de Strasbourg
 */
#ifndef PLAYERCOMMON_H
#define PLAYERCOMMON_H

#define SERVER_NAME "/tmp/mpvbrick"
#define MPV_RUN "mpv /home/mindless/test.mp3 --idle --input-ipc-server=/tmp/mpvbrick >> /tmp/brickoutput"

extern const char mpAll[];
extern const char mpCommand[];
extern const char mpData[];
extern const char mpError[];
extern const char mpEvent[];
extern const char mpSetProp[];
extern const char mpGetProp[];
extern const char mpObserveProp[];


extern const char mpId[];
extern const char mpEvPropChange[];

extern const char mpName[];

extern const char mpPause[];
extern const char mpUnpause[];
extern const char mpIdle[];
extern const char mpPlaybackRestart[];
extern const char mpEnd[];
extern const char mpStart[];
extern const char mpVolume[];

// File related

extern const char mpLoadfile[];
extern const char mpAppendPlay[];
extern const char mpSeek[];
extern const char mpPlaylist[];
extern const char mpPlaylistNext[];
extern const char mpPlaylistPrev[];
extern const char mpMedia[];
extern const char mpTimePos[];
extern const char mpDuration[];
extern const char mpMediaTitle[];
extern const char mpMediaPath[];
extern const char mpFilename[];
extern const char mpCurrent[];
extern const char mpPlaying[];

extern const char mediaUndefined[];

enum messageType{
    error,
    normal,
    warning
};

class PlayerCommon
{
public:
    PlayerCommon();
};

#endif // PLAYERCOMMON_H
