
#include "interface.h"
#include <QApplication>
#include <QTranslator>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QTranslator translator;
    translator.load("translations/app_en.qm");
    a.installTranslator(&translator);
    Interface w;
    if(w.isReady()) w.show();
    else{
        delete &w;
        return a.exec();
    }

    return a.exec();
}
