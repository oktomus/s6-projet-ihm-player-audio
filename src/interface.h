/**
 * Kevin Masson
 * Projet IHM
 * L3 Informatique
 * S6 - Printemps 2017
 * Université de Strasbourg
 */

#ifndef INTERFACE_H
#define INTERFACE_H

#include "ui_interface.h"
#include "lecteur.h"
#include "playercommon.h"
#include "metroslider.h"
#include "potard.h"

#include <QMainWindow>
#include <QWidget>
#include <QCloseEvent>
#include <QTranslator>
#include <QListWidget>

class Lecteur;

namespace Ui {
    class Interface;
}

/**
 * @brief The player's main window class
 */
class Interface : public QMainWindow, private Ui::Interface
                // Mettre l'ui en heritage permet d'acceder directement aux elements
{
    Q_OBJECT

    enum lang{
        fr, en
    };

public:

    /**
     * @brief Default constructor
     * @param parent	  Windows's parent widget
     */
    explicit Interface(QWidget *parent = 0);

    /**
     * @brief Destructor
     */
    ~Interface();

    /**
     * @brief Tells if the ui is ready to be shown or not
     * @return
     */
    bool isReady();

private slots:

     /**
     * @brief Update the player interface
     *
     * @param changement  Indique quel élement a été modifié
     */
    void updateUi(QString changement = mpAll);

    /**
     * @brief Update the list content
     */
    void updatePlaylistContent();

    /**
     * @brief Slot executed when an item is double clicked in the list
     * It changes the current playing music to the music clicked
     */
    void itemListDoubleClicked(QListWidgetItem *);

    /**
     * @brief Show a message to the user
     *
     * @param QString the title
     * @param QString the content
     * @param messageType the type
     */
    void showMessage(QString, QString, messageType);

    /**
     * @brief Slot called when the language is changed
     */
    void changeLanguage(lang);

protected slots:

    void closeEvent(QCloseEvent *event);

private:

    /**
     * @brief Connects required signals and slots
     * Executed on construction only
     */
    void mapEvents();


    /**
     * @brief The lecteur of the application
     */
    Lecteur * m_lecteur;

    /**
   * @brief The french/english translator
   */
    QTranslator m_translator;

    /**
     * @brief Custom widget slider for the timeline
     */
    MetroSlider * m_sliderTimeline;

    /**
     * @brief Custom widget potard for the volume
     */
    Potard * m_potardVolume;


};

#endif // INTERFACE_H
