/**
 * Kevin Masson
 * Projet IHM
 * L3 Informatique
 * S6 - Printemps 2017
 * Université de Strasbourg
 */
#include "playercommon.h"

const char mpAll[] = "all";
const char mpCommand[] = "command";
const char mpData[] = "data";
const char mpError[] = "error";
const char mpEvent[] = "event";
const char mpSetProp[] = "set_property";
const char mpGetProp[] = "get_property";
const char mpObserveProp[] = "observe_property";


const char mpId[] = "id";
const char mpEvPropChange[] = "property-change";

const char mpName[] = "name";

const char mpPause[] = "pause";
const char mpUnpause[] = "unpause";
const char mpIdle[] = "idle";
const char mpPlaybackRestart[] = "playback-restart";
const char mpEnd[] = "end-file";
const char mpStart[] = "start-file";
const char mpVolume[] = "volume";

// File related

const char mpLoadfile[] = "loadfile";
const char mpAppendPlay[] = "append-play";
const char mpSeek[] = "seek";
const char mpPlaylist[] = "playlist";
const char mpPlaylistNext[] = "playlist-next";
const char mpPlaylistPrev[] = "playlist-prev";
const char mpMedia[] = "media";
const char mpTimePos[] = "time-pos";
const char mpDuration[] = "duration";
const char mpMediaTitle[] = "media-title";
const char mpMediaPath[] = "path";
const char mpFilename[] = "filename";
const char mpCurrent[] = "current";
const char mpPlaying[] = "playing";


const char mediaUndefined[] = "undefined";


PlayerCommon::PlayerCommon()
{

}
