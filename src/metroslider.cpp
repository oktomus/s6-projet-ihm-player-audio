/**
 * Joseph Stich
 * Kevin Masson
 * L3 Informatique
 * S6 - Printemps 2017
 * Université de Strasbourg
 */

#include "metroslider.h"

#include <QPainter>
#include <QDebug>

MetroSlider::MetroSlider() : QSlider()
{
    setOrientation(Qt::Horizontal);

}

void MetroSlider::paintEvent(QPaintEvent * paintEv){
    QSlider::paintEvent(paintEv);
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);

    int width = ((double) value() / ((double) maximum()-(double) minimum())) * (double) geometry().width();

    painter.setPen(Qt::NoPen);
    QBrush bg(QColor(20, 20, 20));
    painter.setBrush(bg);
    painter.drawRect(0, 0, geometry().width(), geometry().height());
    QBrush fg(QColor(232, 142, 64));
    painter.setBrush(fg);
    painter.drawRect(0, 0, width, geometry().height());
}
