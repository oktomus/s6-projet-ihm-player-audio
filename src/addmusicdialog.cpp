/**
 * Kevin Masson
 * Projet IHM
 * L3 Informatique
 * S6 - Printemps 2017
 * Université de Strasbourg
 */
#include "addmusicdialog.h"

#include <QEvent>

AddMusicDialog::AddMusicDialog() : QFileDialog()
{
    m_btnOpen = NULL;
    m_listView = NULL;
    m_treeView = NULL;
    m_selectedFiles.clear();

    this->setOption(QFileDialog::DontUseNativeDialog, true);
    this->setFileMode(QFileDialog::Directory);
    QList<QPushButton*> btns = this->findChildren<QPushButton*>();
    for (int i = 0; i < btns.size(); ++i) {
        QString text = btns[i]->text();
        if (text.toLower().contains("open") || text.toLower().contains("choose"))
        {
            m_btnOpen = btns[i];
            break;
        }
    }

    if (!m_btnOpen) return;

    m_btnOpen->installEventFilter(this);
    //connect(m_btnOpen, SIGNAL(changed()), this, SLOT(btnChanged()))
    m_btnOpen->disconnect(SIGNAL(clicked()));
    connect(m_btnOpen, SIGNAL(clicked()), this, SLOT(chooseClicked()));


    m_listView = findChild<QListView*>("listView");
    if (m_listView) {
        m_listView->setSelectionMode(QAbstractItemView::ExtendedSelection);
    }

    m_treeView = findChild<QTreeView*>();
    if (m_treeView) {
        m_treeView->setSelectionMode(QAbstractItemView::ExtendedSelection);
    }

}

bool AddMusicDialog::eventFilter( QObject* watched, QEvent* event )
{
    QPushButton *btn = qobject_cast<QPushButton*>(watched);
    if (btn)
    {
        if(event->type()==QEvent::EnabledChange) {
            if (!btn->isEnabled()) {
                btn->setEnabled(true);
            }
        }
    }

    return QWidget::eventFilter(watched, event);
}

void AddMusicDialog::addFilesFromDir(QString dirPath){
  QDir dir(dirPath);
  QFileInfoList files = dir.entryInfoList();
  foreach (QFileInfo fInfo, files){

      if(fInfo.fileName() == "." || fInfo.fileName() == ".."  || fInfo.fileName() == "") continue;


      if(!fInfo.isFile()){
          addFilesFromDir(fInfo.absoluteFilePath());
      }else{
          m_selectedFiles.append(fInfo.absoluteFilePath());
      }

  }

}


void AddMusicDialog::chooseClicked()
{
    m_selectedFiles.clear();
    QModelIndexList indexList = m_listView->selectionModel()->selectedIndexes();
    foreach (QModelIndex index, indexList)
    {
        if (index.column()== 0)
        {
            QString path = this->directory().absoluteFilePath(index.data().toString());
            QFileInfo fi(path);
            if(!fi.isFile()){
                addFilesFromDir(path);
            }else{
                m_selectedFiles.append(path);
            }
        }
    }

    QDialog::accept();
}

QStringList AddMusicDialog::selectedFiles()
{
    return m_selectedFiles;
}
