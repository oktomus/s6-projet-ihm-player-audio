<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>Interface</name>
    <message>
        <location filename="interface.ui" line="14"/>
        <source>MPV Player - Kevin Masson</source>
        <translation></translation>
    </message>
    <message>
        <source>Titre</source>
        <translation type="vanished">Title</translation>
    </message>
    <message>
        <source>Auteur</source>
        <translation type="vanished">Author</translation>
    </message>
    <message>
        <source>Métadonnées</source>
        <translation type="vanished">Meta</translation>
    </message>
    <message>
        <location filename="interface.ui" line="721"/>
        <source>|&lt;&lt;</source>
        <translation>|&lt;&lt;</translation>
    </message>
    <message>
        <location filename="interface.ui" line="740"/>
        <source>&lt;&lt;</source>
        <translation>&lt;&lt;</translation>
    </message>
    <message>
        <location filename="interface.ui" line="793"/>
        <source>&gt;&gt;</source>
        <translation>&gt;&gt;</translation>
    </message>
    <message>
        <location filename="interface.ui" line="818"/>
        <source>&gt;&gt;|</source>
        <translation>&gt;&gt;|</translation>
    </message>
    <message>
        <location filename="interface.ui" line="848"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="interface.ui" line="867"/>
        <source>Afficher/Masquer liste</source>
        <translation>Show/Hide list</translation>
    </message>
    <message>
        <location filename="interface.ui" line="877"/>
        <source>Radio</source>
        <translation></translation>
    </message>
    <message>
        <source>Lecteur</source>
        <translation type="vanished">Player</translation>
    </message>
    <message>
        <source>Affichage</source>
        <translation type="obsolete">Display</translation>
    </message>
    <message>
        <source>Sourdine</source>
        <translation type="vanished">Mute</translation>
    </message>
    <message>
        <location filename="interface.cpp" line="154"/>
        <source>Lire</source>
        <translation>Play</translation>
    </message>
    <message>
        <source>Aide</source>
        <translation type="vanished">Help</translation>
    </message>
    <message>
        <location filename="interface.ui" line="952"/>
        <source>Langue</source>
        <translation>Language</translation>
    </message>
    <message>
        <source>Liste de lecture</source>
        <translation type="vanished">Playlist</translation>
    </message>
    <message>
        <location filename="interface.ui" line="965"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="interface.ui" line="968"/>
        <source>Open an existing translation file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="interface.ui" line="971"/>
        <source>Ctrl+O</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="interface.ui" line="980"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="interface.ui" line="983"/>
        <source>Save the current translations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="interface.ui" line="986"/>
        <source>Ctrl+S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="interface.ui" line="995"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="interface.ui" line="998"/>
        <source>Need help? Click here!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="interface.ui" line="1007"/>
        <source>Save as...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="interface.ui" line="1010"/>
        <source>Save current translations as...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="interface.ui" line="1013"/>
        <source>Ctrl+Shift+S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="interface.ui" line="1025"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="interface.ui" line="1039"/>
        <source>Français</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="interface.ui" line="1047"/>
        <source>English</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="interface.ui" line="1052"/>
        <source>Vider la liste</source>
        <translation>Clear</translation>
    </message>
    <message>
        <location filename="interface.ui" line="1057"/>
        <source>Ajouter des fichiers</source>
        <translation>Add file(s)</translation>
    </message>
    <message>
        <location filename="interface.ui" line="1062"/>
        <source>Afficher/Masquer la liste</source>
        <translation>Show/Hide list</translation>
    </message>
    <message>
        <location filename="interface.ui" line="1067"/>
        <source>Précedent</source>
        <translation>Previous</translation>
    </message>
    <message>
        <location filename="interface.ui" line="1072"/>
        <source>Play/Pause</source>
        <translation>Play/Pause</translation>
    </message>
    <message>
        <location filename="interface.ui" line="1077"/>
        <source>Suivant</source>
        <translation>Next</translation>
    </message>
    <message>
        <location filename="interface.ui" line="1082"/>
        <source>Quitter</source>
        <translation>Quit</translation>
    </message>
    <message>
        <location filename="interface.ui" line="1087"/>
        <source>A Propos</source>
        <translation>About</translation>
    </message>
    <message>
        <location filename="interface.ui" line="1092"/>
        <source>Raccourcis</source>
        <translation>Shortcuts</translation>
    </message>
    <message>
        <source>Erreur de traduction</source>
        <translation type="vanished">Translation error</translation>
    </message>
    <message>
        <source>Impossible de charger la langue</source>
        <translation type="vanished">Unable to load the language</translation>
    </message>
    <message>
        <location filename="interface.cpp" line="156"/>
        <source>Pause</source>
        <translation>Pause</translation>
    </message>
    <message>
        <location filename="interface.cpp" line="172"/>
        <source>Genre: </source>
        <translation>Type: </translation>
    </message>
    <message>
        <location filename="interface.cpp" line="172"/>
        <source>Date: </source>
        <translation>Date: </translation>
    </message>
</context>
<context>
    <name>Lecteur</name>
    <message>
        <location filename="lecteur.cpp" line="114"/>
        <source>Impossible de se connecter au serveur mpv</source>
        <translation>Unable to connect to MPV</translation>
    </message>
</context>
</TS>
