/**
 * Kevin Masson
 * L3 Informatique
 * S6 - Printemps 2017
 * Université de Strasbourg
 */

#include "potard.h"
#include <QRect>
#include <QPainter>
#include <QWheelEvent>
#include <QMouseEvent>
#include <QTransform>
#include <QDebug>

Potard::Potard(QWidget *parent) :
    QWidget(parent),
    bg(":/img/dial"),
    position(0),
    lastPosSouris(0,0)
{
    setFixedSize(63, 63);
}

int Potard::value(){
    return ((double) position /  360.0) * 100;
}

void Potard::setValue(int v){
    position = ((double) v / 100.0)* 360;
    repaint();
}

void Potard::paintEvent(QPaintEvent * event){
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);
    QTransform trans;
    trans.translate(width()/2, height()/2);
    trans.rotate(position + 90);
    trans.translate(-width()/2, -height()/2);
    painter.setTransform(trans);

    if(m_muted) painter.setOpacity(0.2);


    painter.drawImage(0,0, bg);
}

void Potard::mouseReleaseEvent(QMouseEvent *event){
    if(!mouvementSouris){
        m_muted = !m_muted;
        if( m_muted ) emit muted();
        else emit unmuted();
        repaint();
    }
   mouvementSouris = false;
}

void Potard::mouseMoveEvent(QMouseEvent * event){
  if(event->buttons() == Qt::LeftButton){
    mouvementSouris = true;
    if(m_muted) return;
    if(event->pos().y() < lastPosSouris.y()){
      position += 5;
    }else if(event->pos().y() > lastPosSouris.y()){
      position -= 5;
    }

    lastPosSouris  = event->pos();
    if(position > 360) position = 360;
    if(position < 0) position = 0;
    emit valueChanged();
    repaint();

  }
}

void Potard::mousePressEvent(QMouseEvent * event){
    lastPosSouris  = event->pos();
}

void Potard::wheelEvent(QWheelEvent *event){
    int degrees = event->delta() / 8;

    position += degrees;
    if(position > 360) position = 360;
    if(position < 0) position = 0;
    repaint();
    emit valueChanged();
}
