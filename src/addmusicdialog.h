/**
 * Kevin Masson
 * Projet IHM
 * L3 Informatique
 * S6 - Printemps 2017
 * Université de Strasbourg
 *
 *
 * http://www.qtcentre.org/threads/43841-QFileDialog-to-select-files-AND-folders?p=242803#post242803
 */
#ifndef ADDMUSICDIALOG_H
#define ADDMUSICDIALOG_H

#include <QObject>
#include <QFileDialog>
#include <QListView>
#include <QTreeView>
#include <QPushButton>
#include <QStringList>

class AddMusicDialog : public QFileDialog
{
    Q_OBJECT
private:
    QListView *m_listView;
    QTreeView *m_treeView;
    QPushButton *m_btnOpen;
    QStringList m_selectedFiles;

    void addFilesFromDir(QString);

public slots:
    void chooseClicked();
public:
    AddMusicDialog();
    QStringList selectedFiles();
    bool eventFilter(QObject* watched, QEvent* event);
};

#endif // ADDMUSICDIALOG_H
