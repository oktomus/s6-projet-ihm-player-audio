/**
 * Kevin Masson
 * Projet IHM
 * L3 Informatique
 * S6 - Printemps 2017
 * Université de Strasbourg
 */

#include "interface.h"
#include "playercommon.h"
#include "metroslider.h"

#include <QFile>
#include <QPushButton>
#include <QMessageBox>
#include <QPushButton>
#include <QCloseEvent>
#include <QSlider>
#include <QAction>
#include <QTranslator>
#include <QCoreApplication>
#include <QListWidget>
#include <QSizePolicy>
#include <QLayout>

#ifdef QT_DEBUG
#include <QtDebug>
#endif

Interface::Interface(QWidget *parent) :
    QMainWindow(parent),
    Ui::Interface(),
    m_lecteur(new Lecteur()),
    m_sliderTimeline(new MetroSlider()),
    m_potardVolume(new Potard())
{
    setupUi(this);


    action_layout->addWidget(m_potardVolume);
    layout_timeline->addWidget(m_sliderTimeline);

    // Connections
    mapEvents();

    changeLanguage(fr);

    m_lecteur->init();

}

Interface::~Interface(){
    delete m_lecteur;

}

bool Interface::isReady(){
    return m_lecteur->isRunning();
}

void Interface::mapEvents(){

    connect(this->play, SIGNAL(clicked(bool)), this->m_lecteur, SLOT(togglePlayPause()));

    connect(m_potardVolume, &Potard::valueChanged, this, [this]{
        m_lecteur->changeVolume(m_potardVolume->value());
    });

    connect(m_potardVolume, &Potard::muted, this, [this]{
        m_lecteur->mute();
    });

    connect(m_potardVolume, &Potard::unmuted, this, [this]{
        m_lecteur->unmute();
    });

    connect(m_sliderTimeline, &QSlider::actionTriggered, this, [this]{
        m_lecteur->changeTimePos(m_sliderTimeline->value());
    });

    connect(this->m_lecteur, SIGNAL(playerUpdated(QString)), this, SLOT(updateUi(QString)));

    connect(this->m_lecteur, SIGNAL(message(QString, QString, messageType)), this, SLOT(showMessage(QString, QString, messageType)));

    connect(m_lecteur, SIGNAL(quit()), this, SLOT(close()));


    connect(toggleListe, &QPushButton::clicked, this, [this]{
        if(toggleListe->isChecked()){
            layout()->setSizeConstraint(QLayout::SetFixedSize); // Pour s'assurer que l'ui retressise
            liste->hide();
            resize(sizeHint());
        }else{
            layout()->setSizeConstraint(QLayout::SetMaximumSize);
            liste->show();
            resize(sizeHint());

        }

    });

    connect(actionFrancais, &QAction::triggered, this, [this]{
        changeLanguage(fr);
    });
    connect(actionEnglish, &QAction::triggered, this, [this]{
        changeLanguage(en);
    });

    connect(actionAjouter, SIGNAL(triggered(bool)), m_lecteur, SLOT(addSongs()));
    connect(addSongs, SIGNAL(clicked()), m_lecteur, SLOT(addSongs()));

    connect(next, SIGNAL(clicked(bool)), m_lecteur, SLOT(next()));
    connect(prev, SIGNAL(clicked(bool)), m_lecteur, SLOT(previous()));
    connect(moveUp, SIGNAL(clicked(bool)), m_lecteur, SLOT(incTime()));
    connect(moveDown, SIGNAL(clicked(bool)), m_lecteur, SLOT(decTime()));

    connect(liste, SIGNAL(itemDoubleClicked(QListWidgetItem*)), this, SLOT(itemListDoubleClicked(QListWidgetItem *)));
}

void Interface::changeLanguage(lang lg){
  switch(lg) {
    case en:
      if(!m_translator.load("app_en", ":/lang")){
          showMessage("Impossible d'ouvrir le fichier de traduction anglais", "", error);
          break;
      }
      QApplication::instance()->installTranslator(&m_translator);
      actionEnglish->setChecked(true);
      actionEnglish->setEnabled(false);
      actionFrancais->setChecked(false);
      actionFrancais->setEnabled(true);
      retranslateUi(this);
      break;
  default:
  case fr:
      QApplication::instance()->removeTranslator(&m_translator);
      actionEnglish->setChecked(false);
      actionEnglish->setEnabled(true);
      actionFrancais->setChecked(true);
      actionFrancais->setEnabled(false);
      retranslateUi(this);
      break;
    }
}

void Interface::itemListDoubleClicked(QListWidgetItem * item){
    int currentPlaying = -1;
    QList<Musique> musics = m_lecteur->musics();
    for(int i = 0; i < musics.size(); ++i){
        if(musics.at(i).current){
            currentPlaying = i;
            break;
        }
    }

    if(currentPlaying == -1) return;

    int currentRow = -1;

    for(int i = 0; i < liste->count(); ++i){
        QListWidgetItem * row = liste->item(i);
        if(row == item){
            currentRow = i;
            break;
        }
    }

    if(currentRow == -1) return;

    if(currentPlaying == currentRow) return;

    if(currentPlaying < currentRow){
        for(int j = 0; j < currentRow - currentPlaying; ++j){
          m_lecteur->next();
        }
    }else{
        for(int j = 0; j < currentPlaying - currentRow; ++j){
          m_lecteur->previous();
        }

    }

}


void Interface::showMessage(QString title, QString content, messageType type){
#ifdef QT_DEBUG
  qDebug() << "Showing a Message" ;
#endif
    QMessageBox msgBox;
    msgBox.setText(title + "\n" + content);
    msgBox.exec();
}

void Interface::updateUi(QString changement){
#ifdef QT_DEBUG
  qDebug() << "Update UI " << changement;
#endif

    // Pause
    if(changement == mpAll || changement == mpPause || changement == mpUnpause || changement == mpPlaylist){
      if(!m_lecteur->isPlaying() || m_lecteur->isPaused()){
        this->play->setText(tr("Lire"));
      }else{
        this->play->setText(tr("Pause"));
      }
    }

    // Volume slider
    if(changement == mpAll || changement == mpVolume){
        if(m_potardVolume->value() != m_lecteur->volume() && !m_potardVolume->isMuted()) m_potardVolume->setValue(m_lecteur->volume());
    }

    // Meta
    if((changement == mpAll || changement == mpPlaylist)){
        updatePlaylistContent();
        Musique * m = m_lecteur->getCurrentMusic();
        if(m != NULL){
          title->setText(m->title);
          subtitle->setText(m->artist + ": " + m->album);
          meta->setText(tr("Genre: ") + m->genre + "\n" + tr("Date: ") + m->date);
          if(m_sliderTimeline->maximum() != m->duration) m_sliderTimeline->setMaximum(m->duration);
          int min = m->duration / 60;
          int sec = m->duration - min * 60;
          QString timeStr = QString::number(min) + ":";
          if(sec < 10) timeStr += "0";
          timeStr += QString::number(sec);
          duration->setText(timeStr);
        }
    }


    // Time slider
    if(changement == mpAll || changement == mpPlaylist || changement == mpTimePos){
        Musique * m = m_lecteur->getCurrentMusic();
        if(m != NULL){
          int min = m->timePos / 60;
          int sec = m->timePos - min * 60;
          QString timeStr = QString::number(min) + ":";
          if(sec < 10){
              timeStr += "0";
          }
          timeStr += QString::number(sec);
          timePos->setText(timeStr);
          if(m_sliderTimeline->maximum() != m->duration) m_sliderTimeline->setMaximum(m->duration);
          m_sliderTimeline->setValue(m->timePos);

        }

    }

}

void Interface::updatePlaylistContent(){
    liste->clear();
    for(Musique m : m_lecteur->musics()){
        QListWidgetItem * mItem = new QListWidgetItem();
        mItem->setText(m.title);
        if(m.current){
          QFont ft;
          ft.setBold(true);
          mItem->setFont(ft);
        }
        liste->addItem(mItem);
        if(m.current) liste->setCurrentItem(mItem);
    }

}

void Interface::closeEvent (QCloseEvent *event)
{
    m_lecteur->stop();
    event->accept();
}
