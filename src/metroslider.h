/**
 * Joseph Stich
 * Kevin Masson
 * L3 Informatique
 * S6 - Printemps 2017
 * Université de Strasbourg
 */

#ifndef METROSLIDER_H
#define METROSLIDER_H

#include <QSlider>
#include <QPaintEvent>

class MetroSlider : public QSlider
{
public:
    MetroSlider();

protected:

    void paintEvent(QPaintEvent *);
};

#endif // METROSLIDER_H
