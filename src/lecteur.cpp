/**
 * Kevin Masson
 * Projet IHM
 * L3 Informatique
 * S6 - Printemps 2017
 * Université de Strasbourg
 */
#include "lecteur.h"
#include "playercommon.h"
#include "addmusicdialog.h"

#include <QVariant>
#include <QDateTime>
#include <QList>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QtConcurrent/QtConcurrent>
#include <QFileDialog>
#include <QListView>
#include <QTreeView>
#include "tag.h"
#include "taglib.h"
#include "tpropertymap.h"
#include "audioproperties.h"
#include "fileref.h"
#include "tlist.h"
#include "tstring.h"
#include "tmap.h"

#ifdef QT_DEBUG
#include <QtDebug>
#endif

Lecteur::Lecteur():
  QObject(),
  m_socket(new QLocalSocket(this)),
  waitingData(),
  mutex()
{
#ifdef QT_DEBUG
  qDebug() << "ATTENTION, les debugs peuvent ne pas s'afficher dans le bon ordre du au fonctionnement asynchrone";
#endif
   QDateTime now = QDateTime::currentDateTime();
   m_id = now.toMSecsSinceEpoch();
#ifdef QT_DEBUG
  qDebug() << "Player unique id is " << m_id;
#endif

  m_receptionRunning = true;
  m_messageLoopThread = QtConcurrent::run(this, &Lecteur::messageReceptionLoop);

  m_currentMusic = -1;


  connect(m_socket, &QLocalSocket::disconnected, this, [this]{
      stop();
      quit();
  });


}


Lecteur::~Lecteur(){
    stop();
}

void Lecteur::init(){
  connectOrLaunchMPV();

}

void Lecteur::stop(){
    m_receptionRunning = false;
    if(!m_messageLoopThread.isFinished()) m_messageLoopThread.waitForFinished();
    if(m_socket->state() != QLocalSocket::NotOpen) m_socket->close();
}

bool Lecteur::isPlaying(){
    for(Musique m : m_musics){
        if(m.playing) return true;
    }
    return false;
}

Musique * Lecteur::getCurrentMusic(){
    if(m_currentMusic >= 0 && m_currentMusic < m_musics.size()){
        return &(m_musics[m_currentMusic]);
    }
    return NULL;
}

void Lecteur::connectOrLaunchMPV(){

#ifdef QT_DEBUG
  qDebug() << "Connecting to the socket..";
#endif

  // On essaye de se connecter

  m_socket->connectToServer(SERVER_NAME);

  // Si on ne peut pas se connecter
  if (!m_socket->waitForConnected()) {
#ifdef QT_DEBUG
    qDebug() << "Can't connect";
    qDebug() << m_socket->errorString();
    qDebug() << m_socket->error();
#endif

    // retour erreur et fermeture
    stop();
    emit message(tr("Impossible de se connecter au serveur mpv"), m_socket->errorString(), error);
    return;
  }

  // on recupère les données pour mettre l'interface à jour
  observeUpdates();
  updateFromSocket();

#ifdef QT_DEBUG
        qDebug() << "\n\n";
#endif


}

void Lecteur::observeUpdates(){
#ifdef QT_DEBUG
  qDebug() << "Observe player updates";
#endif
  QJsonObject send;
  QList<QVariant> data;

  // Observe volume
  data.clear();
  data << mpObserveProp << m_id << mpVolume;
  send[mpCommand]= QJsonValue::fromVariant(data);
  sendToSocket(send);

  // Observe pos
  data.clear();
  data << mpObserveProp << m_id << mpTimePos;
  send[mpCommand]= QJsonValue::fromVariant(data);
  sendToSocket(send);

  // Observe playlist
  data.clear();
  data << mpObserveProp << m_id << mpPlaylist;
  send[mpCommand]= QJsonValue::fromVariant(data);
  sendToSocket(send);

}

void Lecteur::updateFromSocket(){

#ifdef QT_DEBUG
  qDebug() << "Read socket initial data";
#endif
  QJsonObject send;
  QList<QVariant> data;


  waitingData.append(mpVolume);
  waitingData.append(mpPause);
  waitingData.append(mpPlaylist);

  // Check volume
  data.clear();
  data << mpGetProp << mpVolume;
  send[mpCommand]= QJsonValue::fromVariant(data);
  sendToSocket(send);

  // Check pause
  data.clear();
  data << mpGetProp << mpPause;
  send[mpCommand]= QJsonValue::fromVariant(data);
  sendToSocket(send);

  // Check playlist
  data.clear();
  data << mpGetProp << mpPlaylist;
  send[mpCommand]= QJsonValue::fromVariant(data);
  sendToSocket(send);

}

QJsonObject Lecteur::takeMessage(){
    QDataStream in(m_socket);
    QString str = in.device()->readLine();
#ifdef QT_DEBUG
    qDebug() << "Recevied " << str;
#endif
    QJsonParseError err;
    QJsonDocument jDoc = QJsonDocument::fromJson(str.toUtf8(), &err);

    return jDoc.object();
}

void Lecteur::parseMessage(QJsonObject jObj){


  // Si c'est un event
  if(jObj.contains(mpEvent)){
      QString evType = jObj.value(mpEvent).toString(); // Prop global change
      bool isId = jObj.contains(mpId);
      if(isId){
         qint64 id = jObj.value(mpId).toVariant().toLongLong();
         if(id != m_id) return;
         else evType = jObj.value(mpName).toString();
      }

      // Real job here
    if(evType == mpPause || evType == mpEnd){  // pause
      m_pause = true;
      emit playerUpdated(mpPause);
    }else if(evType == mpUnpause || evType == mpStart){ // unpause
      m_pause = false;
      emit playerUpdated(mpUnpause);
    }else if(evType == mpPlaybackRestart){ // playback restart
        emit playerUpdated(mpIdle);
        //getMusicInfo();
    }else if(evType == mpPlaylist && jObj.contains(mpData)){
        parsePlaylistMessage(jObj.value(mpData));
        emit playerUpdated(mpPlaylist);
    }else if(evType == mpVolume && jObj.contains(mpData)){
        m_volume = jObj.value(mpData).toInt();
        emit playerUpdated(mpVolume);
    }else if(evType == mpTimePos && jObj.contains(mpData)){
        Musique * current = getCurrentMusic();
        if(current != NULL){
            current->timePos = jObj.value(mpData).toVariant().toInt();
            emit playerUpdated(mpTimePos);
        }
    }

  }else if(jObj.contains(mpData) && waitingData.size() >  0){ // Si c'est une data
    QJsonValue val = jObj.value(mpData);
    QString waiting = waitingData.at(0);


    // Real job here
    if(waiting == mpVolume){
      m_volume = val.toDouble();
      emit playerUpdated(mpVolume);
    }else if(waiting == mpPause || waiting == mpUnpause){
      m_pause = val.toBool();
      emit playerUpdated(mpUnpause);
    }else if(waiting == mpDuration){
        //getCurrentMusic()->duration = val.toInt();
        emit playerUpdated(mpDuration);
    }else if(waiting == mpPlaylist){
        parsePlaylistMessage(val);
        emit playerUpdated(mpPlaylist);
    }

    waitingData.removeAt(0);

  }
}

void Lecteur::parsePlaylistMessage(QJsonValue msg){
    QJsonArray files = msg.toArray();
    for(QJsonValue file : files){
       Musique m;
       QJsonObject map = file.toObject();
       if(map.contains(mpCurrent)) m.current = map.value(mpCurrent).toBool();
       else m.current = false;
       if(map.contains(mpPlaying)) m.playing = map.value(mpPlaying).toBool();
       else m.playing = false;
       m.path = map.value(mpFilename).toString();
       QMap<QString, QString> tags = getTags(m.path);
       QMap<QString, int> props = getProperties(m.path);
       if(tags.contains("ALBUM")) m.album = tags.value("ALBUM");
       if(tags.contains("ARTIST")) m.artist = tags.value("ARTIST");
       if(tags.contains("DATE")) m.date = tags.value("DATE");
       if(tags.contains("GENRE")) m.genre = tags.value("GENRE");
       if(tags.contains("TITLE")) m.title = tags.value("TITLE");
       else{
           QStringList pathSplit = m.path.split("/");
           QStringList extSplit = pathSplit[pathSplit.size() -1].split(".");
           m.title = extSplit[0];
       }
       if(props.contains("length")) m.duration = props.value("length");
       // Check if already here
       int old = -1;
       for(int i = 0; i < m_musics.size(); ++i){
           if(m_musics.at(i).path == m.path){ // Update
               old = i;
               break;
           }
       }
       if(old >= 0){
           Musique * mOld = &(m_musics[old]);
           mOld->current = m.current;
           mOld->playing = m.playing;
       }else{
           m_musics.append(m);
#ifdef QT_DEBUG
           qDebug() << "New music in playlist " <<  tags << props;
#endif
       }
    }

    m_currentMusic = -1;
    for(int i = 0; i < m_musics.size(); ++i){
        if(m_musics.at(i).current){
            m_currentMusic = i;
            break;
        }
    }

}

void Lecteur::messageReceptionLoop(){
    while(m_receptionRunning){
        QDataStream in(m_socket);
        if(in.atEnd()){
            QThread::msleep(100);
            continue;
        }
        QJsonObject jObj = takeMessage();
        parseMessage(jObj);
#ifdef QT_DEBUG
        qDebug() << "\n\n";
#endif
    }

}

void Lecteur::sendToSocket(QJsonObject json){
  QByteArray bytes = QJsonDocument(json).toJson(QJsonDocument::Compact)+"\n";
  if (m_socket!=NULL) {
#ifdef QT_DEBUG
      qDebug() << "Sending " << bytes << "\n";
#endif
    m_socket->write(bytes.data(), bytes.length());
    m_socket->flush();
  }
}

QMap<QString, QString> Lecteur::getTags(QString fileName) {
  QMap <QString, QString> tagMap;
  TagLib::FileRef f(fileName.toLatin1().data());
  if(!f.isNull() && f.tag()) {
    TagLib::PropertyMap tags = f.file()->properties();
    for(TagLib::PropertyMap::ConstIterator i=tags.begin();
        i != tags.end(); ++i) {
        TagLib::String iStr = i->first;
      for(TagLib::StringList::ConstIterator j=i->second.begin();
          j!=i->second.end(); ++j) {
        tagMap[QString::fromStdString(iStr.toCString(true))]
            =QString::fromStdString(j->toCString(true));
      }
    }
  }
  return tagMap;
}

QMap<QString, int> Lecteur::getProperties(QString fileName){
  QMap <QString, int> props;
  TagLib::FileRef f(fileName.toLatin1().data());
  if(!f.isNull() && f.audioProperties()) {
    TagLib::AudioProperties *properties = f.audioProperties();
    props["bitrate"]=properties->bitrate();
    props["sample rate"]=properties->sampleRate();
    props["channels"]=properties->channels();
    props["length"]=properties->length();
  }
  return props;
}

void Lecteur::togglePlayPause(){


#ifdef QT_DEBUG
  qDebug() << "Toggle play";
#endif

  // '{ "command": ["set_property", "pause", false/true] }'

  if(m_musics.size() < 1){
      addSongs();
  }

  QJsonObject jsonObject ;
  QList<QVariant> data;
  data << mpSetProp << mpPause << !m_pause;
  jsonObject[mpCommand]= QJsonValue::fromVariant(data);
  sendToSocket(jsonObject);

}

void Lecteur::changeVolume(int val){
   if(val == m_volume) return;

#ifdef QT_DEBUG
  qDebug() << "Changing volume " << val;
#endif

  // '{ "command": ["set_property", "volume", value] }'

  QJsonObject jsonObject ;
  QList<QVariant> data;
  data << mpSetProp << mpVolume << val;
  jsonObject[mpCommand]= QJsonValue::fromVariant(data);
  sendToSocket(jsonObject);
}

void Lecteur::changeTimePos(int pos){
    if(m_currentMusic < 0 || pos == getCurrentMusic()->timePos) return;
#ifdef QT_DEBUG
  qDebug() << "Changing timePos " << pos;
#endif

  // '{ "command": ["set_property", "time-pos", seconds] }'

  QJsonObject jsonObject ;
  QList<QVariant> data;
  data << mpSetProp << mpTimePos << pos;
  jsonObject[mpCommand]= QJsonValue::fromVariant(data);
  sendToSocket(jsonObject);
}

void Lecteur::addSongs(){
#ifdef QT_DEBUG
  qDebug() << "Add song";
#endif

  AddMusicDialog amd;
  if(amd.exec()){
    QStringList fNames = amd.selectedFiles();


    QJsonObject jsonObject;
    QList<QVariant> data;

    for(QString file : fNames){
      data.clear();
      data << mpLoadfile << file << mpAppendPlay;
      jsonObject[mpCommand]= QJsonValue::fromVariant(data);
      sendToSocket(jsonObject);
    }
  }

}

void Lecteur::next(){
#ifdef QT_DEBUG
  qDebug() << "Play next song";
#endif

  QJsonObject jsonObject ;
  QList<QVariant> data;
  data << mpPlaylistNext;
  jsonObject[mpCommand]= QJsonValue::fromVariant(data);
  sendToSocket(jsonObject);

}

void Lecteur::previous(){
#ifdef QT_DEBUG
  qDebug() << "Play previous song";
#endif

  QJsonObject jsonObject ;
  QList<QVariant> data;
  data << mpPlaylistPrev;
  jsonObject[mpCommand]= QJsonValue::fromVariant(data);
  sendToSocket(jsonObject);

}

void Lecteur::incTime(){
#ifdef QT_DEBUG
  qDebug() << "Go futher in time";
#endif

  QJsonObject jsonObject ;
  QList<QVariant> data;
  data << mpSeek << 10;
  jsonObject[mpCommand]= QJsonValue::fromVariant(data);
  sendToSocket(jsonObject);

}


void Lecteur::decTime(){
#ifdef QT_DEBUG
  qDebug() << "Go futher in past";
#endif

  QJsonObject jsonObject ;
  QList<QVariant> data;
  data << mpSeek << -10;
  jsonObject[mpCommand]= QJsonValue::fromVariant(data);
  sendToSocket(jsonObject);

}

void Lecteur::mute(){
    m_lastVolume = m_volume;
    changeVolume(0);
}

void Lecteur::unmute(){
    if(!m_volume) changeVolume(m_lastVolume);
}
