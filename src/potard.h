/**
 * Kevin Masson
 * L3 Informatique
 * S6 - Printemps 2017
 * Université de Strasbourg
 */

#ifndef POTARD_H
#define POTARD_H

#include <QWidget>
#include <QPaintEvent>
#include <QWheelEvent>
#include <QMouseEvent>
#include <QImage>
#include <QPoint>

/**
 * @brief Custom widget used as a slider
 */
class Potard : public QWidget
{
    Q_OBJECT

public:
    explicit Potard(QWidget *parent = 0);

    /**
     * @brief Return a value depnding of the angle
     * @return an integer between 0 and 100
     */
    int value();

    /**
     * @brief Set the slider value
     */
    void setValue(int);

    /**
     * @brief Check if the potard is muted or not
     * @return
     */
    bool isMuted(){ return m_muted; }

protected:
    void paintEvent(QPaintEvent * event);
    void wheelEvent(QWheelEvent *event);
    void mouseMoveEvent(QMouseEvent * event);
    void mousePressEvent(QMouseEvent * event);
    void mouseReleaseEvent(QMouseEvent * event);

signals:

    /**
     * @brief Signal emitted when the value has changed
     */
    void valueChanged();

    /**
     * @brief Signal emitted when the potard is muted
     */
    void muted();

    /**
     * @brief Signal emitted when the potard is unmuted
     */
    void unmuted();

private:

    /**
     * @brief interface image
     */
    QImage bg;

    /**
     * @brief The mouse last position
     *
     * Used to determine new angle
     */
    QPoint lastPosSouris;

    /**
     * @brief Use to check if the mouse has moved or no
     */
    bool mouvementSouris;

    /**
     * @brief True if the potard is muted
     */
    bool m_muted;

    /**
     * @brief The widget angle (0 < angle < 360)
     */
    int position;

};

#endif // POTARD_H
